<?php
/**
 * Link
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Multivendor\Block;

class Link extends \Magento\Framework\View\Element\Html\Link
{
    protected $_configHelper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magestore\Multivendor\Helper\Config $configHelper,
        array $data = []
    )
    {
        $this->_configHelper = $configHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getHref()
    {
        return $this->_storeManager->getStore()->getUrl('multivendor/vendor/listing');
    }

    public function toHtml()
    {
        if ($this->_configHelper->getStoreConfig('multivendor/general/enable_toplink') == 0) {
            return '';
        } else {
            return parent::toHtml();
        }
    }
}
