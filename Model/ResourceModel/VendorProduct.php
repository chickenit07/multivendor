<?php
/**
 * VendorProduct
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Multivendor\Model\ResourceModel;

class VendorProduct extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('multivendor_vendor_product', 'vendor_product_id');
    }
}
