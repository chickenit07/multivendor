<?php
/**
 * Collection
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Multivendor\Model\ResourceModel\VendorProduct;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('Magestore\Multivendor\Model\VendorProduct',
            'Magestore\Multivendor\Model\ResourceModel\VendorProduct');
    }
}
