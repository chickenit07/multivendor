<?php

namespace Magestore\Multivendor\Plugin\Catalog\Product;
class Collection extends \Magento\Catalog\Model\ResourceModel\Product\Collection
{
    public function beforeGetSize(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $object)
    {
        $object->_totalRecords = null;
    }
}
